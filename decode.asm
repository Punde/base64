;	AUTHOR: Benjamin Schlegel
;	Last modified: 2017-15-12
;	Purpose of this program is to take base64 data from stdin and convert it to a normal string
;
SECTION .bss			; Section containing uninitialized data

	INPUTLEN	equ 16000		; length of input
	Input: 	resb INPUTLEN		; input storage ("string")

	OUTPUTLEN equ 12000			; length of output 
	Output: resb OUTPUTLEN		;output storage ("string")
	
SECTION .data			; Section containing initialised data

		
SECTION .text			; Section containing code

global 	_start			; Linker needs this to find the entry point!
	
_start:
	nop			; This no-op keeps gdb happy...

; Read a Inputer full of text from stdin:
Read:
	mov eax,3		; Specify sys_read call
	mov ebx,0		; Specify File Descriptor 0: Standard Input
	mov ecx,Input		; Pass offset of the Inputer to read to
	mov edx,INPUTLEN		; Pass number of bytes to read at one pass
	int 80h			; Call sys_read to fill the Inputer
	mov ebp,eax		; Save # of bytes read from file for later
	sub ebp,2

; Set up the registers for next steps:
	mov esi,Input		
	mov edi,Output		
	xor ecx,ecx		
	xor edx,edx
    jmp Scan

;Convert a character from 'b64 mode' to 'normal mode'sss
Convert:
    cmp r9b, 61 ; Ascii '='
    je .equals

    cmp r9b, 43 ; Ascii '+'
    je .plus

    cmp r9b, 47 ; ascii '/'
    je .slash

    sub r9b, 48 ; Ascii 0
    cmp r9b, 9 
    jle .digits

    sub r9b, 17 ; Ascii A
    cmp r9b, 25
    jle .done   ;no need to convert upper case

    sub r9b, 32 ; Ascii a
    cmp r9b, 25 
    jle .lower

.equals:
    mov r9b, 0
    jmp .done
.plus:
    add r9b, 19
    jmp .done
.slash:
    add r9b, 16
    jmp .done

.digits:
    add r9b, 52
    jmp .done

.lower:
    add r9b, 26
    jmp .done

.done:
    ret


; Go through the Inputer and convert binary values to hex digits:
Scan:
    xor rax, rax
    push rdx
    mov rdx, 0  
.convertLoop:
    add rdx, rsi                ;for some reason the [] operator does not work with 3 args, so we combine them
	shl r10, 6
	mov r9b, byte [edx+ecx]
    call Convert
    add r10b, r9b
    inc rdx
    sub rdx, rsi
    cmp rdx, 4          ;do the loop four times
    jl .convertLoop

    pop rdx
	push rcx		;Gonna use rcx as counter -> save value for later
	mov rcx, 2		;do this loop 4 times (4 output characters from the 3 input characters)

.loop:
	mov byte [Output+rdx+rcx], r10b
	shr r10d,8
	dec rcx			
	jnl .loop

	pop rcx			;Use rcx as input string pointer to current position again
	add rdx, 3		;increment output pointer
	add rcx, 4		; Increment input string pointer
	cmp rcx,rbp	; Compare to the number of characters in the Inputer
	jna Scan	; Loop back if ecx is <= number of chars in Inputer
	jmp Print

Print:
; Write the line of hexadecimal values to stdout:
	mov al, byte 0x0a
	mov byte [Output+edx+4], al
	mov rax,4		; Specify sys_write call
	mov rbx,1		; Specify File Descriptor 1: Standard output
	mov rcx,Output		; Pass offset of line string
	mov rdx,OUTPUTLEN		; Pass size of the line string
	int 80h			; Make kernel call to display line string
	jmp Done		; Loop back and load file Inputer again

;Exit this program
Done:
	mov eax,1		; Code for Exit Syscall
	mov ebx,0		; Return a code of zero	
	int 80h			; Make kernel call

