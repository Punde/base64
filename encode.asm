;	AUTHOR: Benjamin Schlegel
;	Last modified: 2017-15-12
;	Purpose of this program is to take a string from stdin and convert it to base64 data
;
SECTION .bss			; Section containing uninitialized data

	INPUTLEN	equ 12000		; length of input
	Input: 	resb INPUTLEN		; input storage ("string")

	OUTPUTLEN equ 16000			; length of output 
	Output: resb OUTPUTLEN		;output storage ("string")
	
SECTION .data			; Section containing initialised data
	Dictionary: db "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"	;Mapping for the encoding
	Equalsign: db "="
		
SECTION .text			; Section containing code

global 	_start			; Linker needs this to find the entry point!
	
_start:
	nop			; This no-op keeps gdb happy...

; Read a Input from stdin:
Read:
	mov eax,3		; Specify sys_read call
	mov ebx,0		; Specify File Descriptor 0: Standard Input
	mov ecx,Input		; Pass offset of the Inputer to read to
	mov edx,INPUTLEN		; Pass number of bytes to read at one pass
	int 80h			; Call sys_read to fill the Inputer
	mov ebp,eax		; Save # of bytes read from file for later
	sub ebp,2

; Set up the registers for next steps:
	mov esi,Input		
	mov edi,Output		
	xor ecx,ecx		
	xor edx,edx

; Go through the Input and convert binary values to hex digits:
Scan:
	xor eax,eax		; Clear eax to 0
	
	mov r9b, byte [esi+ecx]

	mov r8b, byte [esi+ecx+1]
	cmp r8b, 10				;If the next Character is \n (10 or 0x0a), thats the end of the string
	je Twice				; -> we have to treat this case differently from the rest
	shl r9, 8				;Make space for the next bit	
	mov r9b, r8b			;move next bit into r9

	mov r8b, byte [esi+ecx+2]
	cmp r8b, 10				;If the next Character is \n (10 or 0x0a), thats the end of the string
	je Once					; -> we have to treat this case differently from the rest
	shl r9, 8				;Make space for the next bit	
	mov r9b, r8b			;move next bit into r9

	push rcx		;Gonna use rcx as counter -> save value for later
	mov rcx, 3		;do this loop 4 times (4 output characters from the 3 input characters)

.loop:
	mov r10d,r9d				
	and r10d,3Fh	;Bit mask for the 6 least significant bits
	mov al, byte [Dictionary+r10d]
	mov byte [Output+rdx+rcx], al
	shr r9d,6
	dec rcx			
	jnl .loop

	pop rcx			;Use rcx as input string pointer to current position again
	add rdx, 4		;increment output pointer
	add rcx, 3		; Increment input string pointer
	cmp rcx,rbp	; Compare to the number of characters in the Inputer
	jna Scan	; Loop back if ecx is <= number of chars in Inputer
	jmp Print


;Adds an equal sign at the end twice
Twice:
	;we want the 6 most significant bits in r10
	mov r10d,r9d				
	shr r10d,2
	mov al, byte [Dictionary+r10d]
	mov byte [Output+rdx], al

	;remaining bits + 4 zeroes so the length is correct
	and r11d, 3
	shl r11d, 4
	mov al, byte [Dictionary+r11d]
	mov byte [Output+edx+1], al

	;Add two = signs
	mov al, byte [Equalsign]
	mov [Output+rdx+2], al
	mov [Output+rdx+3], al
	jmp Print


;Adds an equal sign at the end once
Once:
	mov r10d, r9d
	mov r11d, r9d
	mov r12d, r9d

	; we want the 6 most significant bits in r10
	shr r10d, 10
	mov al, byte [Dictionary+r10d]
	mov byte [Output+edx], al

	;we want the next 6 bits after the ones in r10	
	shr r11d, 4		
	and r11d, 111111b
	mov al, byte [Dictionary+r11d]
	mov byte [Output+edx+1], al

	;we want the remaining bits + 2 zeroes so the length is correct
	and r12d, 1111b
	shl r12d, 2
	mov al, byte [Dictionary+r12d]
	mov byte [Output+edx+2], al
	

	;add an = sign at the end of string
	mov al, byte [Equalsign]
	mov [Output+edx+3], al
	jmp Print



Print:
; Write the line of hexadecimal values to stdout:
	mov al, byte 0x0a
	mov byte [Output+edx+4], al
	mov rax,4		; Specify sys_write call
	mov rbx,1		; Specify File Descriptor 1: Standard output
	mov rcx,Output		; Pass offset of line string
	mov rdx,OUTPUTLEN		; Pass size of the line string
	int 80h			; Make kernel call to display line string
	jmp Done		; Loop back and load file Inputer again

;Exit this program
Done:
	mov eax,1		; Code for Exit Syscall
	mov ebx,0		; Return a code of zero	
	int 80h			; Make kernel call

