all: encode decode

encode: encode.o
	ld -o encode encode.o
encode.o: encode.asm
	nasm -f elf64 -g -F stabs encode.asm

decode: decode.o
	ld -o decode decode.o
decode.o: decode.asm
	nasm -f elf64 -g -F stabs decode.asm
